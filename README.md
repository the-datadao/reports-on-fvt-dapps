# Resume of the several analysis of the finance.vote dapp suite
This repository contains datasets and reports on the FVT dapp suites.

# influence.vote 
Influence.vote is a snapshot voting client with native quadratic voting functionality. influence.vote is designed for token weighted decentralised content curation and rough consensus formation. 
There were several reports on influence.vote:


### 1) A product analysis of the inlfuence.vote dapp
This analysis looks at previous votes and tries to 
The main research questions were: Were there attacks? How could they be dealt with? What is participation? These were some of the questions that this report aimed to answer. 

### Author 
[@Fabs#8423](https://twitter.com/fabsmartins)

### Reviewer
[@Lizzl#8507](https://twitter.com/_datadeo_)

### Files
- [dataset](https://docs.google.com/spreadsheets/d/1KlxVoddkgKlbHBAA_62TFwhIws-O22cgwJ1j68YZ3v8/edit?usp=sharing)
- [report](https://gitlab.com/the-datadao/reports-on-fvt-dapps/-/blob/main/FV_InfluenceDotVote_Report.pdf)


### 2) Analysis of a sybil attack on influence.vote
This analysis takes a look at a sybil attack in influence.vote and compares it to the same vote without the sybil as well as an imaginary vote with a less sensible sybil performing a more targeted attack. This is useful data since it illustrates the importance of sybil resistance. 

### Author
[@Maxet1000#6315](@maxet1000)

### Files
[Report and dataset](https://gitlab.com/the-datadao/reports-on-fvt-dapps/-/blob/main/InfluenceSybilAnalysis_lizzl.xlsx_-_Blad1.csv) 



# markets.vote
Markets.vote is a prediction market and collective intelligence tool, which uses quadratic voting and a decentralised identity system to curate emerging markets and reach consensus on the perceived future performance of assets.
There were several reports on markets:


### 1) Finding Signals in markets.vote

An in-depth analysis of how markets.vote users voted during the first year of the prediction market (and first 30 weeks on BSC). Using historical price data, predicted versus actual performance of tokens was analysed. A series of strategies were plotted to show how individuals could have used the predictions to inform their trades. Interactive graphs allowed people to engage with the data in a more meaningful way than static graphics.

### Author
[@ShiftRunStop#854289](https://twitter.com/ShiftAndRunStop)

### Files
[Finding Signal in markets.vote](https://dry-forest-0623.on.fleek.co/markets_vote_analysis.html)



### 2) A product analysis of the markets.vote dapp
An extensive product analysis of the markets.vote dapp. Main research questions were: How did users interact with the dapp? How did users apply quadratic voting?

### Author
[@Lizzl#8507](https://twitter.com/_datadeo_)

### Files
[Report and datasets](https://github.com/Lizzl/market.vote_analysis)



# yield.vote
yield.vote is a liquidity mining and staking platform, which utilises Harberger Taxes to create “perma-liquidity” and create the potentia for tuneable governance parameters that can be modified by token weighted voting. 

### A product analysis of the yield.vote dapp

### Author
[@Lizzl#8507](https://twitter.com/_datadeo_)

### Files
[Report and datasets](https://github.com/Lizzl/yield.vote-analysis)


## License
The datasets and reports are open for everyone to use. 
If you make amendments to the reports, please get in touch with the authors first.

## Project status
Completed 



